﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MotelApp.Models;

namespace MotelApp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Users> Users { get; set; }

        public DbSet<Town> Town { get; set; }

        public DbSet<Motel> Motels { get; set; }


        public DbSet<Admin> Admin { get; set; }

        public DbSet<Client> Clients { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
             optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=motels;Trusted_Connection=True;MultipleActiveResultSets=true");

    }
}
