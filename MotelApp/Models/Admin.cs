﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotelApp.Models
{
    public class Admin : Users
    {
        public string Role { get; } = "Admin";
    }
}
