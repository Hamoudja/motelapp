﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MotelApp.Models
{
    public class Motel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Aderese { get; set; }
        public string Tel { get; set; }
        public int Places { get; set; }
        public int Stars { get; set; }
        public string Pic_Url { get; set; }

        [ForeignKey("Town")]
        public int Id_Town { get; set; }

        [Display(Name = "Nom de Ville")]
        public Town Town { get; set; }
    }
}
