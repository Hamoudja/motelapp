﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotelApp.Models
{
    public class Client : Users
    {
        public string Role { get; } = "Admin";
    }
}
