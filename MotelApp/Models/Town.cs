﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotelApp.Models
{
    public class Town
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Pic_Url { get; set; }
    }
}
